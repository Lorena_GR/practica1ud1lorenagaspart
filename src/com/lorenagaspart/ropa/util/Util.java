package com.lorenagaspart.ropa.util;
/*
@author loren
@version 1.0
@since 1.8
 */

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class Util {
    /**
     *  Metodo que muestra un mensaje de error
     */
    public static void mensajeError(String mensaje){
        JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
    }
    /**
     * Mensaje de confirmacion
     */
    public static int mensajeConfirmacion(String mensaje, String titulo){
        return JOptionPane.showConfirmDialog(null, mensaje, titulo, JOptionPane.YES_NO_OPTION);
    }
    /**
     * Crear selector de fichero
     */
    public static JFileChooser crearSelectorFichero(File rutaDefecto, String tipoArchivos, String extension){
        JFileChooser selectorFichero = new JFileChooser();
        if(rutaDefecto!=null){
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if(extension!=null){
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivos, extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }
}
