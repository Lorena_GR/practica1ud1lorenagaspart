package com.lorenagaspart.ropa.gui;
import com.github.lgooddatepicker.components.DatePicker;
import com.lorenagaspart.ropa.base.Ropa;
import javax.swing.*;

/**
 * @author loren
 */
public class Ventana {
    /**
     * Aqui creamos todos los componentes que formaran parte de la interfaz de nuestra aplicación
     */
    private JPanel panel1;
    public JRadioButton trajeRadioButton;
    public JRadioButton vestidoRadioButton;
    public JTextField colorTxt;
    public JTextField tallaTxt;
    public JTextField precioTxt;
    public JButton nuevoButton;
    public JButton exportarButton;
    public JButton importarButton;
    public JList list1;
    public JTextField corbAccTxt;
    public JLabel corbataAcceso;
    public JTextField nombreTxt;
    public DatePicker fechaCompraDP;
    public DatePicker fechaEntregaDP;
    public JTextField idTxt;
    public JTextField personaTxt;
    public JTextField estacionTxt;
    public JButton vaciarButton;

    public JFrame frame;

    public DefaultListModel<Ropa> dlmRopa;

    /**
     * Metodo constructor que en el que creamos la interfaz y le damos diferentes funciones como que hacer cuando cerramos
     * Tambien hacerlo visible y darle una ubicación.
     */
    public Ventana(){
        frame = new JFrame("RopaMVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        iniciarComp();
    }
    /*
    Un metodo que inicia la DefaultListModel y la añade a la lista.
     */
    private void iniciarComp(){
        dlmRopa = new DefaultListModel<Ropa>();
        list1.setModel(dlmRopa);
    }

}
