package com.lorenagaspart.ropa.gui;
/**
 * @author Loren
 */

import com.lorenagaspart.ropa.base.Ropa;
import com.lorenagaspart.ropa.base.Traje;
import com.lorenagaspart.ropa.base.Vestido;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class RopaModelo {
    private ArrayList<Ropa> listaRopa;
    public RopaModelo(){
        listaRopa = new ArrayList<Ropa>();
    }

    public ArrayList<Ropa> obtenerRopa(){
        return listaRopa;
    }

    /**
     *
     */
    public void altaTraje(String id, String nombre, String persona, String estacion, String color, float talla,
                          float precio, LocalDate fechaCompra, LocalDate fechaEntrega, String corbata){
        Traje nuevoTraje = new Traje(id, nombre, persona, estacion, color, talla, precio, fechaCompra, fechaEntrega, corbata);
        listaRopa.add(nuevoTraje);
    }

    /**
     * Nos permite crear un
     */

    public void altaVestido(String id, String nombre, String persona, String estacion, String color, float talla,
                          float precio, LocalDate fechaCompra, LocalDate fechaEntrega, String accesorios){
        Traje nuevoVestido = new Traje(id, nombre, persona, estacion, color, talla, precio, fechaCompra, fechaEntrega, accesorios);
        listaRopa.add(nuevoVestido);
    }

    public boolean existeId(String id){
        for (Ropa unaPrenda : listaRopa){
            if(unaPrenda.getId().equals(id)){
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo que permite exportar los campos, cogiendolos desde el ArrayList en el que se han guardado, por lo que va cogiendo los diferentes elementos por posiciones y así ir pegandolos
     * en un archivo xml.
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        Element raiz = documento.createElement("Prendas");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoRopa = null;
        Element nodoDatos = null;
        Text texto = null;

        for (Ropa unaPrenda: listaRopa){
            if(unaPrenda instanceof Traje){
                nodoRopa = documento.createElement("Traje");
            }else{
                nodoRopa = documento.createElement("Vestido");
            }
            raiz.appendChild(nodoRopa);

            nodoDatos = documento.createElement("id");
            nodoRopa.appendChild(nodoDatos);
            texto = documento.createTextNode(unaPrenda.getId());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("nombre");
            nodoRopa.appendChild(nodoDatos);
            texto = documento.createTextNode(unaPrenda.getNombre());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("persona");
            nodoRopa.appendChild(nodoDatos);
            texto = documento.createTextNode(unaPrenda.getPersona());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("estacion");
            nodoRopa.appendChild(nodoDatos);
            texto = documento.createTextNode(unaPrenda.getEstacion());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("color");
            nodoRopa.appendChild(nodoDatos);
            texto = documento.createTextNode(unaPrenda.getColor());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("talla");
            nodoRopa.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(unaPrenda.getTalla()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("precio");
            nodoRopa.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(unaPrenda.getPrecio()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-compra");
            nodoRopa.appendChild(nodoDatos);
            texto = documento.createTextNode(unaPrenda.getFechaCompra().toString());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-entrega");
            nodoRopa.appendChild(nodoDatos);
            texto = documento.createTextNode(unaPrenda.getFechaEntrega().toString());
            nodoDatos.appendChild(texto);

            if(unaPrenda instanceof Traje) {
                nodoDatos = documento.createElement("corbata");
                nodoRopa.appendChild(nodoDatos);
                texto = documento.createTextNode(((Traje) unaPrenda).getCorbata());
                nodoDatos.appendChild(texto);
            }else{
                nodoDatos = documento.createElement("accesorios");
                nodoRopa.appendChild(nodoDatos);
                texto = documento.createTextNode(((Vestido)unaPrenda).getAccesorios());
                nodoDatos.appendChild(texto);
            }

            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(fichero);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);
        }
    }

    /**
     * Agarra un archivo xml y va recogiendo cada elemento del archivo xml para ir pegandolos uno detras de otro.
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException{
        listaRopa = new ArrayList<Ropa>();
        Traje nuevoTraje = null;
        Vestido nuevoVestido = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList lisaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i <lisaElementos.getLength(); i++){
            Element nodoRopa = (Element) lisaElementos.item(i);

            if(nodoRopa.getTagName().equals("Traje")){
                nuevoTraje = new Traje();
                nuevoTraje.setId(nodoRopa.getChildNodes().item(0).getTextContent());
                nuevoTraje.setNombre(nodoRopa.getChildNodes().item(1).getTextContent());
                nuevoTraje.setPersona(nodoRopa.getChildNodes().item(2).getTextContent());
                nuevoTraje.setEstacion(nodoRopa.getChildNodes().item(3).getTextContent());
                nuevoTraje.setColor(nodoRopa.getChildNodes().item(4).getTextContent());
                nuevoTraje.setTalla(Float.parseFloat(nodoRopa.getChildNodes().item(5).getTextContent()));
                nuevoTraje.setPrecio(Float.parseFloat(nodoRopa.getChildNodes().item(6).getTextContent()));
                nuevoTraje.setFechaCompra(LocalDate.parse(nodoRopa.getChildNodes().item(7).getTextContent()));
                nuevoTraje.setFechaEntrega(LocalDate.parse(nodoRopa.getChildNodes().item(8).getTextContent()));
                nuevoTraje.setCorbata(nodoRopa.getChildNodes().item(9).getTextContent());
                listaRopa.add(nuevoTraje);
            }else if(nodoRopa.getTagName().equals("Vestido")){
                nuevoVestido = new Vestido();
                nuevoVestido.setId(nodoRopa.getChildNodes().item(0).getTextContent());
                nuevoVestido.setNombre(nodoRopa.getChildNodes().item(1).getTextContent());
                nuevoVestido.setPersona(nodoRopa.getChildNodes().item(2).getTextContent());
                nuevoVestido.setEstacion(nodoRopa.getChildNodes().item(3).getTextContent());
                nuevoVestido.setColor(nodoRopa.getChildNodes().item(4).getTextContent());
                nuevoVestido.setTalla(Float.parseFloat(nodoRopa.getChildNodes().item(5).getTextContent()));
                nuevoVestido.setPrecio(Float.parseFloat(nodoRopa.getChildNodes().item(6).getTextContent()));
                nuevoVestido.setFechaCompra(LocalDate.parse(nodoRopa.getChildNodes().item(7).getTextContent()));
                nuevoVestido.setFechaEntrega(LocalDate.parse(nodoRopa.getChildNodes().item(8).getTextContent()));
                nuevoVestido.setAccesorios(nodoRopa.getChildNodes().item(9).getTextContent());
                listaRopa.add(nuevoVestido);
            }
        }
    }

}
