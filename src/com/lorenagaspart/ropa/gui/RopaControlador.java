package com.lorenagaspart.ropa.gui;

import com.lorenagaspart.ropa.base.Ropa;
import com.lorenagaspart.ropa.base.Traje;
import com.lorenagaspart.ropa.base.Vestido;
import com.lorenagaspart.ropa.base.Vestido;
import com.lorenagaspart.ropa.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

/**
 * @author loren
 */
public class RopaControlador implements ActionListener, ListSelectionListener, WindowListener {
    /**
     * Creamos la ventana, modelo y un archivo que guardara la ruta en la que se encuentre el ultimo archivo que importamos
     */
    private Ventana vista;
    private RopaModelo modelo;
    private File ultimaRutaExportada;

    /**
     * Controlador
     * @param vista
     * @param modelo
     */
    public RopaControlador(Ventana vista, RopaModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            System.out.println("No existe el fichero de configuracion" + e.getMessage());
        }
        //añadimos listener
        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    /**
     * Metodo que carga los datos de configuracion desde un archivo llamado ropa.conf
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("ropa.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    /**
     * Metodo que actualiza la ruta exportada
     * @param ultimaRutaExportada
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Guarda los datos de configuración
     * @throws IOException
     */
    private void guardarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());

        configuracion.store(new PrintWriter("ropa.conf"),
                "Datos configuracion ropa");
    }

    /**
     * Metodo que nos permite darle una función a cada uno de los botones que hemos añadido a la interfaz
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios: \n" + "Id \n Nombre \n Persona \n Estacion \n Color \n" +
                            "Talla \n Precio \n FechaCompra \n FechaEntrega" + vista.corbAccTxt.getText());
                    break;
                }

                if (modelo.existeId(vista.idTxt.getText())) {
                    Util.mensajeError("Ya existe una prenda con este id\n" +
                            vista.corbAccTxt.getText());
                    break;
                }
                if (vista.trajeRadioButton.isSelected()) {
                    modelo.altaTraje(vista.idTxt.getText(),
                            vista.nombreTxt.getText(),
                            vista.personaTxt.getText(),
                            vista.estacionTxt.getText(),
                            vista.colorTxt.getText(),
                            Float.parseFloat(vista.tallaTxt.getText()),
                            Float.parseFloat(vista.precioTxt.getText()),
                            vista.fechaCompraDP.getDate(),
                            vista.fechaEntregaDP.getDate(),
                            vista.corbAccTxt.getText());

                } else {
                    modelo.altaVestido(vista.idTxt.getText(),
                            vista.nombreTxt.getText(),
                            vista.personaTxt.getText(),
                            vista.estacionTxt.getText(),
                            vista.colorTxt.getText(),
                            Float.parseFloat(vista.tallaTxt.getText()),
                            Float.parseFloat(vista.precioTxt.getText()),
                            vista.fechaCompraDP.getDate(),
                            vista.fechaEntregaDP.getDate(),
                            vista.corbAccTxt.getText());
                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":
                JFileChooser selectorFichero =
                        Util.crearSelectorFichero(ultimaRutaExportada,
                                "Archivos XML","xml");
                int opt=selectorFichero.showOpenDialog(null);
                if (opt==JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (SAXException e1) {
                        e1.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2= Util.crearSelectorFichero(ultimaRutaExportada,"Archivos XML","xml");
                int opt2=selectorFichero2.showSaveDialog(null);
                if (opt2==JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (TransformerException e1) {
                        e1.printStackTrace();
                    }
                }
                break;
            case "Vaciar":
                DefaultListModel model = (DefaultListModel)vista.list1.getModel();
                model.clear();
            case "Traje":
                vista.corbataAcceso.setText("Corbata");
                break;
            case "Vestido":
                vista.corbataAcceso.setText("Accesorios");
                break;
        }
    }

    /**
     *  Metodo que solo ejecuta el codigo si el valor se está ajustando
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {
            Ropa prendaSeleccionada= (Ropa) vista.list1.getSelectedValue();
            vista.idTxt.setText(String.valueOf(prendaSeleccionada.getId()));
            vista.nombreTxt.setText(prendaSeleccionada.getNombre());
            vista.personaTxt.setText(prendaSeleccionada.getPersona());
            vista.estacionTxt.setText(prendaSeleccionada.getEstacion());
            vista.colorTxt.setText(prendaSeleccionada.getColor());
            vista.tallaTxt.setText(String.valueOf(prendaSeleccionada.getTalla()));
            vista.precioTxt.setText(String.valueOf(prendaSeleccionada.getPrecio()));
            vista.fechaCompraDP.setText(String.valueOf(prendaSeleccionada.getFechaCompra()));
            vista.fechaEntregaDP.setText(String.valueOf(prendaSeleccionada.getFechaEntrega()));

            if (prendaSeleccionada instanceof Traje) {
                vista.trajeRadioButton.doClick();
                vista.corbAccTxt.setText(String.valueOf(((Traje) prendaSeleccionada).getCorbata()));
            } else {
                vista.vestidoRadioButton.doClick();
                vista.corbAccTxt.setText(String.valueOf(((Vestido) prendaSeleccionada).getAccesorios()));
            }

        }

    }

    /**
     * Listener para los botones y los radioButtons
     * @param listener
     */
     private void addActionListener(ActionListener listener) {
        vista.vaciarButton.addActionListener(listener);
        vista.trajeRadioButton.addActionListener(listener);
        vista.vestidoRadioButton.addActionListener(listener);
        vista.exportarButton.addActionListener(listener);
        vista.importarButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
    }

    /**
     * Listener de la ventana
     */
    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    /**
     * listener de la lista
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    /**
     * Metodo que limpia los campos cuando presionamos el boton nuevo, ya que luego este metodo se llama en el actionlistener de nuevo
     */
    private void limpiarCampos() {
        vista.idTxt.setText(null);
        vista.nombreTxt.setText(null);
        vista.personaTxt.setText(null);
        vista.estacionTxt.setText(null);
        vista.colorTxt.setText(null);
        vista.tallaTxt.setText(null);
        vista.precioTxt.setText(null);
        vista.corbataAcceso.setText(null);
        vista.fechaCompraDP.setText(null);
        vista.fechaEntregaDP.setText(null);
        vista.idTxt.requestFocus();
    }

    /**
     * Metodo que te returna true si alguno de los campos estan vacios y false si todos los campos han sido rellenados correctamente
     * @return un booleano
     */
    private boolean hayCamposVacios() {
        if (vista.idTxt.getText().isEmpty() ||
                vista.nombreTxt.getText().isEmpty() ||
                vista.personaTxt.getText().isEmpty() ||
                vista.estacionTxt.getText().isEmpty() ||
                vista.colorTxt.getText().isEmpty() ||
                vista.tallaTxt.getText().isEmpty() ||
                vista.precioTxt.getText().isEmpty() ||
                vista.corbataAcceso.getText().isEmpty() ||
                vista.fechaCompraDP.getText().isEmpty() ||
                vista.fechaEntregaDP.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Carga los datos de la lista
     */
    public void refrescar() {
        vista.dlmRopa.clear();
        for (Ropa unaPrenda : modelo.obtenerRopa()) {
            vista.dlmRopa.addElement(unaPrenda);
        }
    }

    /**
     * Evento que ocurre cuando vas a cerrar la ventana en la que le das el valor al mensaje de confirmacion
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea cerrar la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarDatosConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


    @Override
    public void windowOpened(WindowEvent e) {

    }
}