package com.lorenagaspart.ropa;

import com.lorenagaspart.ropa.gui.RopaControlador;
import com.lorenagaspart.ropa.gui.RopaModelo;
import com.lorenagaspart.ropa.gui.Ventana;

import java.io.IOException;

public class Principal {
    public static void main(String[] args) throws IOException {
        Ventana ventana = new Ventana();
        RopaModelo modelo = new RopaModelo();
        RopaControlador controlador = new RopaControlador(ventana, modelo);
    }
}
