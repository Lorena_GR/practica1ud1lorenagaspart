package com.lorenagaspart.ropa.base;

import java.time.LocalDate;

public class Traje extends Ropa{
    /**
     * Variable creada solo para la clase Traje
     */
    private String corbata;

    /**
     * Constructores que traeb el super y las variables de su herencia
     */
    public Traje(){
        super();
    }

    public Traje(String id, String nombre, String persona, String estacion, String color, float talla, float precio, LocalDate fechaCompra, LocalDate fechaEntrega, String corbata) {
        super(id, nombre, persona, estacion, color, talla, precio, fechaCompra, fechaEntrega);
        this.corbata=corbata;
    }

    /**
     * Getter y setter
     * @return corbata
     */
    public String getCorbata() {
        return corbata;
    }

    public void setCorbata(String accesorios) {
        this.corbata = corbata;
    }

    /**
     *
     * @return toString
     */
    public String toString() {
        return "Vestido{" +
                "id" + getId() +
                "nombre: " + getNombre() +
                "persona: " + getPersona() +
                "estacion: " + getEstacion() +
                "color: " + getColor() +
                "talla: " + getTalla() +
                "precio: " + getPrecio() +
                "fechaCompra: " + getFechaCompra() +
                "fechaEntrega: " + getFechaEntrega() +
                "corbata: '" + getCorbata() + '\'' +
                '}';
    }
}
