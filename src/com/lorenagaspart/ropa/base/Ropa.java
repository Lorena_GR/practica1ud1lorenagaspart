package com.lorenagaspart.ropa.base;

import java.time.LocalDate;

public class Ropa {
    /**
     *Creamos las variables que formaran parte de cada prenda y que usaran las clases que hereden de esta.
     */
    private String id;
    private String nombre;
    private String persona;
    private String estacion;
    private String color;
    private float talla;
    private float precio;
    private LocalDate fechaCompra;
    private LocalDate fechaEntrega;
    public Ropa(){

    }
    /**
     * Constructor
     */
    public Ropa(String id, String nombre, String persona, String estacion, String color, float talla, float precio, LocalDate fechaCompra, LocalDate fechaEntrega) {
        this.id = id;
        this.nombre = nombre;
        this.persona = persona;
        this.estacion = estacion;
        this.color = color;
        this.talla = talla;
        this.precio = precio;
        this.fechaCompra = fechaCompra;
        this.fechaEntrega = fechaEntrega;
    }
    /**
     *Getters y setters
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getTalla() {
        return talla;
    }

    public void setTalla(float talla) {
        this.talla = talla;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float pracio) {
        this.precio = pracio;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public LocalDate getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(LocalDate fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }
}
