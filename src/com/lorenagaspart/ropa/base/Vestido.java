package com.lorenagaspart.ropa.base;

import java.time.LocalDate;

public class Vestido extends Ropa{
    /**
     * VARIABLE PERTENECIENTE A VESTIDO
     */
    public String accesorios;

    /**
     * Constructores que llevaran el super para traer las variables de la herencia
     */
    public Vestido(){
        super();
    }
    public Vestido(String id, String nombre, String persona, String estacion, String color, float talla, float precio, LocalDate fechaCompra, LocalDate fechaEntrega, String accesorios) {
        super(id, nombre, persona, estacion, color, talla, precio, fechaCompra, fechaEntrega);
        this.accesorios=accesorios;
    }

    /**
     * Getters y setteres
     * @return la variable
     */
    public String getAccesorios() {
        return accesorios;
    }

    public void setAccesorios(String accesorios) {
        this.accesorios = accesorios;
    }

    /**
     *
     * @return el toString
     */
    public String toString() {
        return "Vestido{" +
                "id: " + getId() +
                "nombre: " + getNombre() +
                "persona: " + getPersona() +
                "estacion: " + getEstacion() +
                "color: " + getColor() +
                "talla: " + getTalla() +
                "precio: " + getPrecio() +
                "fechaCompra: " + getFechaCompra() +
                "fechaEntrega: " + getFechaEntrega() +
                "accesorios: '" + getAccesorios() + '\'' +
                '}';
    }
}
